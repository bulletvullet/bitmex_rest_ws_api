Simple bitmex rest-api and websocket gateway
Used: django, django-rest-framework, bitmex, channels

## Getting Started
```
1. ~ manage.py createsuperuser
2. ~ manage.py runserver
2. Next you need to create test account bitmex on 127.0.0.1:8000/admin
3. Account name will be used for api requests.

After starting project will be available:
http://127.0.0.1:8000/api/ - rest api.
ws://127.0.0.1:8000/ws/bitmex/<account_name> - ws api
```

## Built With

* [django](https://www.djangoproject.com/) - Web framework
* [django-rest-framework](https://www.django-rest-framework.org/) - Web framework
* [bitmex](https://pypi.org/project/bitmex/) - python api for bitmex trade found
* [channels](https://pypi.org/project/channels/) - Add some async for your django

## Authors

* **Eduard Kucheryaviy** - [bulletvullet](https://bitbucket.org/bulletvullet/)

## License

This project is licensed under the MIT License

