from rest_framework.routers import DefaultRouter
from src.api.views import OrderViewSet

router = DefaultRouter()
router.register(r'orders', OrderViewSet, base_name='orders')
urlpatterns = router.urls
