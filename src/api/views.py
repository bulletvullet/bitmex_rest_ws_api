import json
import bitmex
from rest_framework import viewsets
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from ..bitmex.models import Account
from .serializers import OrderSerializer


class OrderViewSet(viewsets.ViewSet):
    client = None

    def account_check(func):
        def wrapper(*args, **kwargs):
            params = args[1].query_params
            name = params.get('account')
            account = get_object_or_404(Account.objects.all(), name=name)
            args[0].client = bitmex.bitmex(test=True, api_key=account.api_key, api_secret=account.api_secret)
            return func(*args, **kwargs)
        return wrapper

    @account_check
    def create(self, request):
        pre_serializer = OrderSerializer(data=request.data)
        if pre_serializer.is_valid():
            data = self.client.Order.Order_new(**request.data).result()[0]
            serializer = OrderSerializer(data)
            return Response(serializer.data)
        else:
            return Response(pre_serializer.errors)

    @account_check
    def list(self, request):
        data = self.client.Order.Order_getOrders(count=5, reverse=True).result()[0]
        serializer = OrderSerializer(data, many=True)
        return Response(serializer.data)

    @account_check
    def retrieve(self, request, pk=None):
        data = self.client.Order.Order_getOrders(filter=json.dumps({"orderID": pk})).result()[0][0]
        serializer = OrderSerializer(data)
        return Response(serializer.data)

    @account_check
    def destroy(self, request, pk=None):
        data = self.client.Order.Order_cancel(orderID=pk).result()[0][0]
        print(data)
        serializer = OrderSerializer(data)
        return Response(serializer.data)
