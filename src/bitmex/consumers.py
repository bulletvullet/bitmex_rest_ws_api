import errno
import hashlib
import hmac
import json
import time
import urllib
import websocket
from threading import Event, Thread
from channels.generic.websocket import WebsocketConsumer
from src.bitmex.models import Account

BITMEX_URL = "wss://testnet.bitmex.com"
ENDPOINT = "/realtime"
VERB = "GET"


def bitmex_signature(apiSecret, verb, url, nonce, postdict=None):
    """Given an API Secret key and data, create a BitMEX-compatible signature."""
    data = ''
    if postdict:
        data = json.dumps(postdict, separators=(',', ':'))
    parsed_url = urllib.parse.urlparse(url)
    path = parsed_url.path
    if parsed_url.query:
        path = path + '?' + parsed_url.query
    message = (verb + path + str(nonce) + data).encode('utf-8')
    print("Signing: %s" % str(message))
    signature = hmac.new(apiSecret.encode('utf-8'), message, digestmod=hashlib.sha256).hexdigest()
    print("Signature: %s" % signature)
    return signature


class BitmexConsumer(WebsocketConsumer):
    ws_client = None
    account = None

    def bitmex_ws_receive(self):
        while self.event.is_set():
            try:
                message = self.ws_client.recv()
                proxy_data = json.loads(message)
                data = proxy_data.get('data')
                if proxy_data and data:
                    price = data[0].get('markPrice', None)
                    msg = {
                        'timestamp': data[0]['timestamp'],
                        'account': self.account.name,
                        'symbol': data[0]['symbol'],
                        'price': price
                    }
                    self.send(text_data=json.dumps(msg))
            except json.decoder.JSONDecodeError:
                pass
            except OSError as e:
                if e.errno == errno.EBADF:
                    pass

    def connect(self):
        print(dir(self))
        try:
            acc_name = self.scope['url_route']['kwargs']['uri']
            self.account = Account.objects.get(name=acc_name)
        except Account.DoesNotExist as e:
            print(e)
        else:
            self.accept()

    def disconnect(self, close_code):
        print('Disconnected Bitmex socket')
        self.event.clear()
        self.ws_client.close()

    def receive(self, text_data):
        data = json.loads(text_data)
        print(data)
        if data['action'] == 'subscribe':
            expires = int(time.time()) + 5
            signature = bitmex_signature(self.account.api_secret, VERB, ENDPOINT, expires)
            self.ws_client = websocket.create_connection(f"{BITMEX_URL}{ENDPOINT}?api-expires={expires}"
                                                         f"&api-signature={signature}&api-key={self.account.api_key}")
            self.ws_client.send(json.dumps({"op": "subscribe", "args": "position"}))
            self.event = Event()
            self.event.set()
            Thread(target=self.bitmex_ws_receive, args=()).start()
        if data['action'] == 'unsubscribe':
            self.ws_client.send(json.dumps({"op": "unsubscribe", "args": "position"}))
            self.event.clear()
            self.ws_client.close()
